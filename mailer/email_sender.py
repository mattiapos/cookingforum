import asyncio

from fastapi_mail import FastMail, ConnectionConfig, MessageSchema

import config

mail_config = ConnectionConfig(
    MAIL_USERNAME=config.EMAIL_USERNAME,
    MAIL_PASSWORD=config.EMAIL_PASSWORD,
    MAIL_FROM=config.EMAIL_FROM,
    MAIL_PORT=config.EMAIL_PORT,
    MAIL_SERVER=config.EMAIL_SERVER,
    MAIL_FROM_NAME=config.EMAIL_FROM_NAME,
    MAIL_SSL_TLS=config.EMAIL_SSL_TLS,
    MAIL_STARTTLS=config.EMAIL_STARTTLS,
    USE_CREDENTIALS=config.EMAIL_USE_CREDENTIALS,
    TEMPLATE_FOLDER=config.EMAIL_TEMPLATE_FOLDER
)


def send_email(recipient: str, subject: str, body: str):
    message = MessageSchema(
        subject=subject,
        recipients=[recipient],
        body=body,
        subtype='html',
    )

    fm = FastMail(mail_config)

    loop = asyncio.new_event_loop()
    coroutine = fm.send_message(message)
    loop.run_until_complete(coroutine)
