from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, DateTime, PrimaryKeyConstraint
from sqlalchemy.orm import relationship

from db.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    first_name = Column(String)
    last_name = Column(String)
    tfa_enabled = Column(Boolean, default=False)

    tfa_codes = relationship("TwoFactorCode", back_populates="user")


class TwoFactorCode(Base):
    __tablename__ = "tfa_codes"
    __table_args__ = (
        PrimaryKeyConstraint('user_id', 'code'),
    )

    code = Column(String, unique=True)
    expiration_datetime = Column(DateTime, nullable=False)
    user_id = Column(Integer, ForeignKey("users.id"))

    user = relationship("User", back_populates="tfa_codes")
