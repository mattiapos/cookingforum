from datetime import datetime, timedelta
from typing import Type, List

from sqlalchemy import and_
from sqlalchemy.orm import Session

import config
from api import schemas, utils
from db import models
from db.models import User, TwoFactorCode


def add_user(db: Session, user: schemas.UserRegister) -> models.User:
    """
    Add a new user to DB.
    :param db: The DB session.
    :param user: The UserRegister instance with the user to add.
    :return: The user just created.
    """
    new_user = models.User(
        email=user.email,
        password=utils.hash_password(user.password),
        first_name=user.first_name or None,
        last_name=user.last_name or None,
        tfa_enabled=user.tfa_enabled,
    )
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


def find_user_by_id(user_id: int, db: Session):
    """
    Find a user by email.
    :param user_id:
    :param db: The DB session.
    :return: The corresponding user.
    """
    return db.query(models.User).filter_by(id=user_id).one()


def find_user_by_email(db: Session, email: str) -> Type[User]:
    """
    Find a user by email.
    :param db: The DB session.
    :param email: The email to search for.
    :return: The user if it exists and None otherwise.
    """
    return db.query(models.User).filter_by(email=email).first()


def authenticate(email: str, password: str, db: Session) -> User | None:
    """
    Check if email and password for a user are correct.
    :param email:
    :param password:
    :param db:
    :return:
    """
    user = db.query(models.User).filter_by(email=email).first()
    if utils.verify_password(password, user.password):
        return user
    return None


def get_user_codes(user_id: int, db: Session) -> list[str]:
    """
    Retrieves all generated codes for a given user.
    :param user_id: The ID of the user.
    :param db: The DB Session.
    :return: The list of existing OTP codes for the user.
    """
    codes = db.query(models.TwoFactorCode).filter_by(user_id=user_id).all()
    if codes:
        return [item.code for item in codes]
    return []


def add_two_factor_code(user_id: int, code: str, db: Session) -> TwoFactorCode:
    new_tfa = models.TwoFactorCode(
        code=code,
        user_id=user_id,
        expiration_datetime=datetime.utcnow() + timedelta(minutes=config.OTP_CODE_EXPIRATION_MINUTES)
    )
    db.add(new_tfa)
    db.commit()
    db.refresh(new_tfa)
    return new_tfa


def create_otp(user: models.User, db: Session) -> TwoFactorCode:
    current_time = datetime.utcnow()
    valid_otp_exists = db.query(TwoFactorCode).filter(and_(
        TwoFactorCode.user_id == user.id,
        TwoFactorCode.expiration_datetime > current_time
    )).count()
    if valid_otp_exists:
        db.query(TwoFactorCode).filter(and_(
            TwoFactorCode.user_id == user.id,
            TwoFactorCode.expiration_datetime > current_time
        )).update({"expiration_datetime": current_time})
        db.commit()
    otp = utils.get_user_otp(user)
    return add_two_factor_code(user.id, otp, db)


def check_otp(user_id: int, otp: str, db: Session) -> bool:
    current_time = datetime.utcnow()

    valid_otp = db.query(TwoFactorCode).filter(and_(
        TwoFactorCode.user_id == user_id,
        TwoFactorCode.code == otp,
        TwoFactorCode.expiration_datetime > current_time
    ))
    if valid_otp.count() != 1:
        return False
    valid_otp.update({"expiration_datetime": current_time})
    db.commit()
    return True
