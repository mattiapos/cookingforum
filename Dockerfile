FROM python:3.11

COPY ./requirements.txt /cookingforum/requirements.txt

WORKDIR /cookingforum

RUN pip install --no-cache-dir --upgrade  -r /cookingforum/requirements.txt

COPY . .

CMD ["uvicorn", "api.main:app", "--host", "0.0.0.0", "--port", "80"]