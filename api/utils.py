import secrets
import string
from datetime import datetime, timedelta

from jose import jwt
from passlib.context import CryptContext

from config import SECRET_KEY, ALGORITHM
from db import models
from mailer import email_sender

crypt_context = CryptContext(schemes=["bcrypt"])


def verify_password(plain_password, hashed_password) -> bool:
    return crypt_context.verify(plain_password, hashed_password)


def hash_password(plain_password):
    return crypt_context.hash(plain_password)


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def create_otp() -> str:
    otp = ''.join(secrets.choice(string.digits) for _ in range(8))
    return otp


def get_user_otp(authenticated_user: models.User) -> str:
    existing = True
    current_otp = None
    while existing:
        current_otp = create_otp()
        existing = current_otp in [item.code for item in authenticated_user.tfa_codes]

    return current_otp


def send_otp(user: models.User, otp: models.TwoFactorCode):
    email_sender.send_email(user.email, "Login OTP Code", otp.code)
