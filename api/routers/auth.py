from datetime import timedelta
from typing import Generator, Union

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from starlette import status

import config
from api import schemas, utils
from api.schemas import Token
from db import repository
from db.database import SessionLocal


def db_instance() -> Generator:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


router = APIRouter()


@router.post("/register", response_model=schemas.User)
def register_user(user: schemas.UserRegister, db: Session = Depends(db_instance)):
    """
    Register a new user.
    :param user: The User to register.
    :param db: The session to the DB.
    :return: The created User or an Exception
    """
    existing_user = repository.find_user_by_email(db, email=user.email)
    if existing_user is not None:
        raise HTTPException(status_code=409, detail="Email already in use")
    return repository.add_user(db, user)


@router.post("/login", response_model=Union[schemas.Token, dict[str, str]])
def login(credentials: schemas.UserLogin, db: Session = Depends(db_instance)) -> Token | dict[str, str]:
    """
    Login a user.
    :param credentials: The UserLogin schema containing the email and password of the user.
    :param db: The session to the DB.
    :return: An access token
    """
    authenticated_user = repository.authenticate(credentials.email, credentials.password, db)
    if not authenticated_user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    if not authenticated_user.tfa_enabled:
        access_token = utils.create_access_token({
            "sub": authenticated_user.email,
            "name": f"{authenticated_user.first_name} {authenticated_user.last_name}".strip()
        }, expires_delta=timedelta(config.ACCESS_TOKEN_EXPIRATION_MINUTES))
        return Token(access_token=access_token)

    otp = repository.create_otp(authenticated_user, db)
    utils.send_otp(authenticated_user, otp)
    return {"message": f"2FA code sent for user ID {authenticated_user.id}"}


@router.post("/login/tfa", response_model=schemas.Token)
def confirm_tfa(user_id: int, otp: str, db: Session = Depends(db_instance)) -> schemas.Token:
    """
    Check a user OTP.
    :param user_id: The ID of the user.
    :param otp:
    :param db: The session to the DB.
    :return: A JWT access token
    """
    authenticated_user = repository.find_user_by_id(user_id, db)
    if not repository.check_otp(user_id, otp, db):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid OTP",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = utils.create_access_token({
        "sub": authenticated_user.email,
        "name": f"{authenticated_user.first_name} {authenticated_user.last_name}".strip()
    }, expires_delta=timedelta(config.ACCESS_TOKEN_EXPIRATION_MINUTES))
    return Token(access_token=access_token)
