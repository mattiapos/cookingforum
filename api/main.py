from fastapi import FastAPI

from api.routers import auth
from db import models
from db.database import engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
app.include_router(auth.router)
