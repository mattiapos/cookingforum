from typing import Optional

from pydantic import BaseModel, ConfigDict


class UserID(BaseModel):
    id: str


class UserBase(BaseModel):
    email: str
    first_name: Optional[str]
    last_name: Optional[str]
    tfa_enabled: bool


class UserRegister(UserBase):
    password: str


class User(UserBase, UserID):
    model_config = ConfigDict(from_attributes=True)

    id: int


class UserLogin(BaseModel):
    email: str
    password: str


class Token(BaseModel):
    access_token: str
