# Cooking Forum
## Launch local server (no docker)
```
pip install -r requirements.txt
uvicorn api.main:app --host 0.0.0.0 --port 80
```
Swagger documentation: [http://0.0.0.0:80/docs](http://0.0.0.0:80/docs)

## Launch with Docker compose
```
docker compose up --build
```
Swagger documentation: [http://0.0.0.0:80/docs](http://0.0.0.0:80/docs)

## Run tests
```
pytest
```
