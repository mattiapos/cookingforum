from typing import Generator

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool

from api.main import app
from api.routers.auth import db_instance
from db.database import Base

# Using in memory DB for tests
DATABASE_URL = "sqlite://"

engine = create_engine(
    DATABASE_URL,
    connect_args={"check_same_thread": False},
    poolclass=StaticPool,
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)


# Create a test database session
def test_db_session():
    session = TestingSessionLocal()
    yield session
    session.close()



app.dependency_overrides[db_instance] = test_db_session

client = TestClient(app)


def test_register():
    expected_json = {
        "email": "test@test.com",
        "first_name": "Test",
        "last_name": "test",
        "tfa_enabled": True,
    }
    response = client.post("/register", json={
        "password": "testpwd",
        **expected_json
    })
    assert response.status_code == 200
    assert expected_json.items() <= response.json().items()


def test_login_tfa_disabled():
    expected_json = {
        "email": "test@test.com",
        "first_name": "Test",
        "last_name": "test",
        "tfa_enabled": False,
    }
    response = client.post("/register", json={
        "password": "testpwd",
        **expected_json
    })
    assert response.status_code == 200
    assert expected_json.items() <= response.json().items()

    response = client.post("/login", json={
        "email": "test@test.com",
        "password": "testpwd",
    })
    assert response.status_code == 200
    assert "access_token" in response.json().keys()


def test_login_tfa_enabled():
    expected_json = {
        "email": "test@test.com",
        "first_name": "Test",
        "last_name": "test",
        "tfa_enabled": True,
    }
    response = client.post("/register", json={
        "password": "testpwd",
        **expected_json
    })
    assert response.status_code == 200
    assert expected_json.items() <= response.json().items()

    response = client.post("/login", json={
        "email": "test@test.com",
        "password": "testpwd",
    })
    assert response.status_code == 200
    assert "message" in response.json()
    assert "2FA code sent for user ID" in response.json()["message"]
